1/ What is Git?

  - Git is a software that keep track of changes that you make to files, directories.
  
  - Git is open source, free, fast and reliable.
  
  - History of Git : SCCS (1972), RSC (1982), CVS (1986 - 1990), SVN (2000), BitKeeper SCM (2000 - important).

2/ Installing Git:

* Configuring Git:

    -  System: 
  
	    + File: /etc/gitconfig
	    + `git config --system`
	
    - User:
  
	    + File: ~/.gitconfig
	    + `git config --global`
	
    - Project:
  
	    + File: my_project/.git/config
	    + `git config`

3/ Getting started:

  - Basic flow:
  
	+ Initialize: `git init`
	
	+ Make changes
	
	+ Add the changes: `git add .`
	
	+ Commit the changes: `git commit -m "message"`
	
4/ Git Concepts and Architecture:

- Three-trees architecture:

  - Repository
  
  - Staging index
  
  - Working directory
  
- HEAD: 

    - Pointer to "tip" of current branch
  
    - Pointer to parent of next commit (when commits occur).

5/ Making Changes to Files:

- Viewing changes: `git diff`

- Deleting files : `git rm <file>`

- Moving and renaming files: `git mv <file1> <file2>`

- Ex: 
    + rename file a.txt to b.txt -> git mv a.txt b.txt
    + move file a.txt to directory/a.txt -> git mv a.txt directory/a.txt

6/ Using Git with a Real Project:

-`git diff --color-words` - Comparison mode: highlight the different words side by side. Default mode: highlight the different lines separately.

-`git commit -am "message"` - add all changes and commit. However, untracked and deleted files not included.

7/ Undoing Changes:

- Undoing working directory changes: `git checkout -- <file>`

- Unstaging files: `git reset HEAD <file>`

- Amending commits: `git commit --amend -m "message"` -> can add the changes to previous commit or change commit messages

- Retrieving old versions: `git checkout <SHA_value> -- <file>`

- Reverting a commit: `git revert <SHA_value>`

- Undo commits: `git reset <option> <SHA_value>`

    +`--soft`: not change staging index or working directory
  
    +`--mixed`: changes staging index to match repository and not change working directory
  
    +`--hard`: changes staging index and working directory
  
- Removing untracked files: `git clean`

8/ Ignoring Files:

- Using .gitignore files:

    + Basic expressions: * ? [aeiou] [0-9]
  
    + Negate expressions with !
  
    + Ignore the whole directory: Aa/Bb/
  
    + comment, blank lines are skipped
  
- What to ignore:

    + compiled source code
  
    + packages and compressed files
  
    + logs and databases
  
    + operating system generated files
  
    + user-uploaded assets (images, PDFs, videos)
  
- Should see: 

    + https://help.github.com/articles/ignoring-files/
  
    + https://github.com/github/gitignore
  
- User-specific ignore: `git config --global core.excludesfile <.gitignore file location>`

- Ignoring tracked files: `git rm --cached <file>`

9/ Navigating the commit tree:

- Tree-ish: a reference to the commit. Some ways to reference:

    + Full SHA-1 hash
  
    + Short SHA-1 hash
  
    + at least 4 characters
  
    + unambiguous (8-10 or even more, dependent on the size of the project)
  
    + HEAD pointer
  
    + Branch reference, tag reference
  
    + Ancestry
  
    + Using caret: HEAD/SHA hash/master + ^/^^/^^^/... (the number of caret = the number of commits to trace back). E.g. master^ (parent), acf87504^^ (grandparent)...
  
    + Using the tilde: HEAD~ + the number of commits to trace back. E.g. HEAD~3
  
- Exploring tree listings:

    + blod: file	
  
    + tree: directory
  
    + list a tree: `git ls-tree <tree-ish> [path]...`
  
    + usefull command: git help ls-tree
  
- Getting more from the commit log: `git log [options]`

    +`--oneline` - oneline list
  
    +`-<number>` - limit the number of shown commit
  
    +`since/until/before/...="<time>"` - E.g. "2017-08-03", "2 days/weeks ago", 2.weeks, ...
  
    +`--grep="<string>"` - Regular expression search
  
    +`<SHA>..<SHA>` - List range
  
    +`<SHA>..<SHA> <file>` - Commit relevant to the file in that range
  
    +`-p - Patch` - Show a diff of the changes in each commit
  
    +`--format=<format`- e.g. oneline/short/medium/full>
  
- Viewing commit: `git show [options] <object>...`

- Comparing commit: `git diff <SHA_value>..<SHA_value>`

10/ Branching:

- Branches is used to try new ideas, isolate features, ...

- Creating new branch: `git branch <new branch name>`

- Switching branch: `git checkout <branch>`

- Creating and switching at the same time: `git checkout -b <new branch name>`

- Comparing branch: `git diff <branch>..<branch>` - Caret can also be used to compare with a branch's ancestor

- Renaming branch: git branch -m/--move <old name> <new_name>

- Deleting branch: git branch -d/--delete <branch to delete>

    * Notes: 
  
        * Cannot delete current branch
    
        * Cannot delete branch that has changes that aren't merged
    
        * Force delete: git branch -D <branch to delete>

11/ Merging Branches:

- Merging branches: `git merge [options] <branch to merge with>`

    * Options:
  
        +--no-ff: No fast-forward merging (no silent merging)
    
        +--ff-only: Only fast-forward merging
    
        +--abort : abort merge process
    
    * If branch-name completely contain current branch, do fast-forward merge; else recursive-merge and commit.
    
- Merging conflicts: conflict occurs when two changes to the same line or set of lines in two different commits.

- Resolving merge conflict:

    - Abort merge: git merge --abort
  
    - Resolve manually
  
    - Use a merge tool
  
- Strategies to reduce merge conflicts:

    - Keep lines short
  
    - Keep commits small and focused
    
    - Beware of stray edits to whitespace (spaces, tabs, line returns)
    
    - Merge often
    
    - Track changes to master: Keep sync with master branch

12/ Stashing Changes:

- The stash is

    + a place where can store changes temporarily without having to commit them to the repository.
  
    + A special fourth area in Git
  
    + Work similarly to commit, but without SHA
  
- Saving changes into the stash: `git stash save "<message>"`

- Viewing list changes in the stash: `git stash list`

- Viewing a change in the stash: `git stash show [/p] <stash item>`

- Retrieving changes from stash:

    + Pull the change and remove it from the stash: `git stash pop <stash item>` 
  
    + Pull the change and leave a copy in the stash: `git stash apply <stash item>`
  
    + If <stash item> not specified, it will be treated as stash@{0}
  
    + In case of merging conflicts, stash item will be preserved even if pop is used
  
- Delete stash items: `git stash drop <stash item>`

- Delete everything in the stash: `git stash clear`

13/ Remotes:

- remote: Git repository stored on server, other people can see.

- origin/master: Git repository stored on local machine, reference to remote branch server.

- Adding a remote repository: `git remote add <alias> <url>`

- Removing remote: `git remote rm <alias>`

- Creating a remote branch: `git push -u <alias> <branch>`

- Cloning a remote repository: `git clone <url> [folder_name]`

- Tracking remote branches: push with -u option or clone it.

- Pushing changes to a remote: `git push <alias> <branch>`

- Fetching changes from a remote reposity: `git fetch <alias> <branch>`

    + fetch before working
  
    + fetch before pushing
  
    + fetch often
  
- Merging in fetched changes: git pull = git fetch + git merge

- Deleting a remote branch: `git push <remote> :<remote branch> or git push <remote> --delete <remote branch>`

14/ Tools and Next Steps:

- Setting up alias:

    + Edit the config file directly, or git config --global alias.<shortkey> "<command>"
  
- Git hosting companies: github, bitbucket, gitorious

